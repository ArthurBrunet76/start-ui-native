1 : edition des pipelines sur le CI/CD de gitlab

2 : une image node:16.13.0 est utiliser pour les pipelines

3 : dans le stage build la commande npm install --legacy-peer-deps
est utilisé pour éviter les problémes d'incompatibilité avec certain module npm.

4 : modification du test dans le fichier src/tests/App.test.js

5 : creation d'un cache pour les différents jobs

6 : configuration du git pour l'envoie du fichier a versionner
